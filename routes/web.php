<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::post('/api/user/login', 'ApiController@login');
route::post('/api/user/register', 'ApiController@register');



Route::get('/', function () {
    return view('welcome');
});

route::group(['middleware' => 'jwt.auth'], function() {
    route::post('/api/user/logout', 'ApiController@logout');
    route::get('/api/user/getuser', 'ApiController@getAuthenticatedUser');

    
    route::get('/api/invoices/{invoice_id}', 'InvoiceController@getUserInvoicebyId');
    route::get('/api/invoice/search/invoices', 'InvoiceController@getUserInvoices');


    route::get('/api/invoice/search/week', 'InvoiceController@getUserInvoicesofWeek');
    route::get('/api/invoice/search/month', 'InvoiceController@getUserInvoicesofMonth');
    route::get('/api/invoice/search/day', 'InvoiceController@getUserInvoicesCurrentDay');


    route::post('/api/invoice/insert/', 'InvoiceController@CreateNewInvoice');
    route::post('/api/invoice/InvoiceType/', 'InvoiceController@getInvoicebyInvoiceType');
    route::post('/api/invoice/InvoiceCategory/', 'InvoiceController@getInvoicebyCategoryId');

    route::post('/api/insurence/user/', 'InvoiceInsurenceController@GetInsurencesByUser');
    route::post('/api/insurence/insert/', 'InvoiceInsurenceController@CreateNewInvoice');
    route::post('/api/insurence/search/{InsurencebyId}/', 'InvoiceInsurenceController@getInsurencebyId');


    route::post('/api/images_invoices/insert/', 'InvoiceImagesController@SendInvoiceImage');
});


