<?php
namespace App\Models;

use Eloquent;
use DB;

Class Categories Extends Eloquent{

    protected $table = 'categories';

    protected $fillable = array('id, name, image_url');

    public $timestamps = false;

    
    public function categories_invoice(){
        return  $this->hasMany('App\Models\Invoices');
    }

    

 

}
