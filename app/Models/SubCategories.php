<?php
namespace App\Models;

use Eloquent;
use DB;

Class SubCategories Extends Eloquent{

    protected $table = 'subcategories';

    protected $fillable = array('id, name, image_url');

    public $timestamps = false;

    
    public function subcategories_invoice(){
        return  $this->hasMany('App\Models\Invoices');
    }


}
