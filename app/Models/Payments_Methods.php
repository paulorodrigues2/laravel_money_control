<?php
namespace App\Models;

use Eloquent;
use DB;

Class Payments_Methods Extends Eloquent{

    protected $table = 'payment method';

    protected $fillable = array('id, name, image_url');

    public $timestamps = false;

    
    public function payments_invoices(){
        return  $this->hasMany('App\Models\Invoices');
    }

 

}
