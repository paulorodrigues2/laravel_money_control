<?php
namespace App\Models;

use Eloquent;
use DB;

Class Country Extends Eloquent{

    protected $table = 'country';

    protected $fillable = array('id, name');

    public $timestamps = false;

    
    public function country_invoice(){
        return  $this->hasMany('App\Models\Invoices');
    }

 

}
