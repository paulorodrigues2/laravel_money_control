<?php
namespace App\Models;

use Eloquent;
use DB;

Class Invoice_Images Extends Eloquent{

    protected $table = 'invoice_images';

    protected $fillable = array('id, name_url, Date_Created, Invoice_id, user_id');

    public $timestamps = false;

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice', 'Invoice_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }


}
