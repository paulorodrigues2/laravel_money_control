<?php
namespace App\Models;

use Eloquent;
use DB;

Class Invoice_Type Extends Eloquent{

    protected $table = 'invoice_type';

    protected $fillable = array('id, name');

    public function invoice(){
           return $this->hasMany('App\Models\Invoice');
    }

}
