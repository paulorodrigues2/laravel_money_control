<?php
namespace App\Models;

use Eloquent;
use DB;

Class Invoice Extends Eloquent{

    protected $table = 'invoice';

    protected $fillable = array('id, Date_Created, name, date_invoice, state, Categories_id, Payment_Method_id, Country_id, amount, Entity_name, SubCategories_id, user_id, Cities_id, Store_name_id, invoice_type_id');

    public function users(){
           return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function countries(){
            return $this->belongsTo('App\Models\Country', 'Country_id');
    }

    public function payment_methods()
    {
        return $this->belongsTo('App\Models\Payments_Methods', 'Payment_Method_id');
    }

    public function subcategories()
    {
        return $this->belongsTo('App\Models\SubCategories', 'SubCategories_id');
    }

    public function categories()
    {
        return $this->belongsTo('App\Models\Categories', 'Categories_id');
    }

    public function stores()
    {
        return $this->belongsTo('App\Models\Stores', 'Store_name_id');
    }

    public function cities()
    {
        return $this->belongsTo('App\Models\Cities', 'Cities_id');
    }

     public function invoice_images()
     {
         return $this->hasMany('App\Models\Invoice_Images');
     }

     public function insurences(){
        return $this->hasMany('App\Models\Insurence_Invoice');
    }

    public function invoice_type(){
        return $this->belongsTo('App\Models\Invoice_Type', 'invoice_type_id');
    }

}
