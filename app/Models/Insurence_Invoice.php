<?php
namespace App\Models;

use Eloquent;
use DB;

Class Insurence_Invoice Extends Eloquent{

    protected $table = 'insurence_invoice';

    protected $fillable = array('id, date_created, updated_created , date_limit , invoice_id');

    public function users(){
           return $this->belongsTo('App\Models\Invoice', 'invoice_id');
    }

}
