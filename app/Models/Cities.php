<?php
namespace App\Models;

use Eloquent;
use DB;

Class Cities Extends Eloquent{

    protected $table = 'cities';

    protected $fillable = array('id, name');

    public $timestamps = false;

    public function city_invoice(){
        return  $this->hasMany('App\Models\Invoices');
    }
}
