<?php
namespace App\Models;

use Eloquent;
use DB;

Class Stores Extends Eloquent{

    protected $table = 'store';

    protected $fillable = array('id, name, image_store');

    public $timestamps = false;

    
    public function store_invoice(){
        return  $this->hasMany('App\Models\Invoices');
    }

 

}
