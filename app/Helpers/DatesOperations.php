<?php
namespace App\Helpers;
/**
 * Class ErrorsDisplay.
 */
class DatesOperations
{
   public function getDaysOfWeek($start, $end){
        $dates = [];

        $date = $start;
        while ($date <= $end) {
            if (! $date->isWeekend()) {
                $dates[] = $date->copy();
            }
            $date->addDays(1);
        }
        return $dates;
    }
}