<?php
namespace App\Helpers;
/**
 * Class ErrorsDisplay.
 */
class ErrorsDisplay
{
   public function isError($errors, $name){
        if($errors->has($name)){
            return '<span class="help-block"><strong>'.$errors->first($name).'</strong></span>';
        }
    }
}