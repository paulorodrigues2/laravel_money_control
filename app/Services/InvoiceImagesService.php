<?php
namespace App\Services;
use App\Repositories\InvoiceImagesRepository;

class InvoiceImagesService
{
    private $repository_invoice_images;

    public function __construct( InvoiceImagesRepository $repository_invoice_images )
    {
        $this->InvoiceImagesRepository = $repository_invoice_images;
    }

    public function insertImageInvoice($user_id, $image_path, $invoice_id)
    {
        $query = $this->InvoiceImagesRepository->create($user_id, $image_path, $invoice_id);
        return $query;
    }
    
}