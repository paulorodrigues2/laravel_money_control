<?php
namespace App\Services;
use App\Repositories\InvoiceRepository as InvoiceRepository;

class InvoiceServices
{
    private $repository_invoice;

    public function __construct( InvoiceRepository $repository_invoice )
    {
        $this->InvoiceRepository = $repository_invoice;
    }

    public function findInvoiceUserbyId($user_id, $invoice_id)
    {
        $query = $this->InvoiceRepository->findByUserInvoiceId($user_id, $invoice_id);
        return $query;
    }
    
    public function findInvoiceUserFilter($user_id, $filter, $order, $page, $type_order, $filter_id)
    {
        $query = $this->InvoiceRepository->findInvoicesByUserWithFilters($user_id, $filter, $order, $page, $type_order, $filter_id);
        return $query;
    }

    public function findInvoiceWeek($user_id, $from, $to){
        $query = $this->InvoiceRepository->findInvoicesByWeekUser($user_id, $from, $to);
        return $query;
    }

    public function findInvoicesofMonth($user_id){
        $query = $this->InvoiceRepository->findInvoicesByMonthUser($user_id);
        return $query;
    }

    public function findInvoicesDay($user_id, $day){
        $query = $this->InvoiceRepository->getInvoicesDayByUser($user_id, $day);
        return $query;
    }

    public function findByCategory($user_id, $category_id){
        $query = $this->InvoiceRepository->findInvoicesByCategory($user_id, $category_id);
        return $query;
    }

    public function findInvoicesByInvoice_Type($user_id, $invoice_type_id){
        $query = $this->InvoiceRepository->findInvoicesByInvoice_Type($user_id, $invoice_type_id);
        return $query;
    }

    public function createInvoice($data){
        $query = $this->InvoiceRepository->create($data);
        return $query;
    }
    
}