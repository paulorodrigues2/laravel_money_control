<?php
namespace App\Services;
use App\Repositories\Insurence_Invoice_Repository;

class Insurence_InvoiceService
{
    private $repository_insurence_invoice;

    public function __construct( Insurence_Invoice_Repository $repository_insurence_invoice )
    {
        $this->Insurence_Invoice_Repository = $repository_insurence_invoice;
    }

    public function insertInsurence($user_id, $date_limit, $invoice_id)
    {
        $query = $this->Insurence_Invoice_Repository->create($user_id, $date_limit , $invoice_id);
        return $query;
    }

    public function GetInsurencesById($user_id, $InsurencebyId)
    {
        $query = $this->Insurence_Invoice_Repository->GetInsurencesById($user_id, $InsurencebyId);
        return $query;
    }

    public function GetInsurencesByUser($user_id)
    {
        $query = $this->Insurence_Invoice_Repository->GetInsurencesByUser($user_id);
        return $query;
    }
    
}