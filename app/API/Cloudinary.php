<?php
namespace App\API;
use JD\Cloudder\Facades\Cloudder;

class Cloudinary 
{
    public function uploadImage($image_name){
        $image_url = "";
        try {
            Cloudder::upload($image_name, null);

            list($width, $height) = getimagesize($image_name);
     
            $image_url= Cloudder::show(Cloudder::getPublicId(), ["width" => $width, "height"=>$height]);
     
        } catch (\Exception $er) {
           Log::info($er);
        }
        return $image_url;
    }

}