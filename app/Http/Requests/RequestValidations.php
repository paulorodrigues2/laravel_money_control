<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Rules\StringIsLengthRule;

/**
 * Class RequestValidations.
 */
class RequestValidations extends Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     *
     * @return array
     */

    public function rules()
    {
        
        $rules = array();

        if($this->request->has('email')){
            $rules['email'] = ['required|text', new StringIsLengthRule($this->request->get('email'))];
        }

        if($this->request->has('email')){
            $rules['email'] = ['required|text', new StringIsLengthRule($this->request->get('email'))];
        }
            
        return $rules;
    }
}