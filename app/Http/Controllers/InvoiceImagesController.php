<?php
namespace App\Http\Controllers;

use App\Services\InvoiceImagesService;
use App\Services\InvoiceServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Mail; 
use Config;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject as JWTSubject;
use Illuminate\Support\Facades\Hash;
use App\API\Cloudinary;


class InvoiceImagesController extends Controller
{
    private $InvoiceImagesService;

    private $InvoiceServices;

    private $Cloudinary;


    public function __construct( 
    InvoiceImagesService $invoice_image_Services, 
    InvoiceServices $invoiceServices, 
    Cloudinary $cloudinary
    )
    {
        $this->invoice_image_Services = $invoice_image_Services;
        $this->invoiceServices = $invoiceServices;
        $this->cloudinary = $cloudinary;
    }

    public function SendInvoiceImage(Request $request){
        $invoices = [];
        $message = NULL;
        $user_id = JWTAuth::user();

        $validator = Validator::make($request->all(),[
            'files.*'=>'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
            'invoice_id' => 'required|integer'
        ]);

        if($validator->fails()){
            $message = array('message' => $validator);
                    return response()->json($message, 400);
        }
 
        $invoice_id = $request->invoice_id;

        try {
           $checkExistInvoice = $this->invoiceServices->findInvoiceUserbyId($user_id->id, $invoice_id);
           if($checkExistInvoice != NULL){


            $images = $request->file('files');
            $haveError = false;
                if ($request->hasFile('files')) {
                        foreach ($images as $item){
                            if(!$haveError){
                                $image_name =  $item->getRealPath();
                                $get_image_uploaded_url = $this->cloudinary->uploadImage($image_name);

                                if($get_image_uploaded_url != ""){
                                    $insertImage = $this->invoice_image_Services->insertImageInvoice($user_id->id, $get_image_uploaded_url, $invoice_id);
                                }else{
                                    $haveError = true;
                                    $message = array('message' => 'Error saving image' );
                                }
                            }else{
                                break;
                            }
                        }

                        if(!$haveError){
                            $message = array('message' => 'Insert files with success' );
                            return response()->json($message, 200); 
                        }else{
                            $message = array('message' => 'Error upload files' );
                            return response()->json($message, 400); 
                        }
                }else{
                    $message = array('message' => 'Need file to Insert file to upload' );
                    return response()->json($message, 400);
                }
       

           }else{
                $message = array('message' => 'invoice don\t exist' );
                return response()->json($message, 400);
           }

        } catch (\Exception $er) {
            \Log::info($er);
        }
    }
}