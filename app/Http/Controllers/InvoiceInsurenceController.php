<?php
namespace App\Http\Controllers;

use App\Services\Insurence_InvoiceService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Mail; 
use Config;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject as JWTSubject;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Insurence_Invoice_Repository;
use App\Models\Stores;


class InvoiceInsurenceController extends Controller
{
    private $Insurence_InvoiceService;

    private $InvoiceRepository;


    public function __construct( Insurence_InvoiceService $insurence_InvoiceService, Insurence_Invoice_Repository  $insurenceInvoice_Repository )
    {
        $this->Insurence_InvoiceService = $invoiceServices;
        $this->InvoiceRepository = $insurenceInvoice_Repository;
    }

    public function getInsurencebyId($InsurencebyId){
        $invoices = [];
        $message = NULL;
        $user_id = JWTAuth::user();

        try {
           if ($user_id != NULL) {

             $validator = Validator::make($request->all(),[
                'category_id' => 'required|integer'
            ]);
    
            if($validator->fails()){
                $message = array('message' => $validator);
                        return response()->json($message, 400);
            }

              $invoices = $this->Insurence_InvoiceService->GetInsurencesById($user_id->id, $InsurencebyId);

              if(count($invoices) > 0){
                   return response()->json($invoices, 200);
              }else{
                  $message = array('message' => 'Not found any invoice from user' );
                   return response()->json($message , 400);
              }
           }else{
                  $message = array('message' => 'needs to have a user_id' );
                  return response()->json($message, 400);
            }
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            $message = array('message' => 'ERROR on the system, try later' );
            return response()->json($message, 400);

        }
    }

    public function GetInsurencesByUser() {
        $invoices = [];
        $message = NULL;
        $user_id = JWTAuth::user();

        try {
           if ($user_id != NULL) {

            if($validator->fails()){
                $message = array('message' => $validator);
                        return response()->json($message, 400);
            }

            $invoices = $this->invoiceServices->GetInsurencesByUser($user_id)

              if(count($invoices) > 0){
                   return response()->json($invoices, 200);
              }else{
                  $message = array('message' => 'Not found any invoice from user' );
                   return response()->json($message , 400);
              }
           }else{
                  $message = array('message' => 'needs to have a user_id' );
                  return response()->json($message, 400);
            }
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            $message = array('message' => 'ERROR on the system, try later' );
            return response()->json($message, 400);

        }
    }

    public function CreateNewInvoice(Request $request){
        $invoices = [];
        $message = NULL;
        $user_id = JWTAuth::user();

        try {
           if ($user_id != NULL) {

            $validator = Validator::make($request->all(),[
                'date_limit' => 'required|date',
                'invoice_id' => 'required|integer'
            ]);
    
            if($validator->fails()){
                $message = array('message' => $validator);
                        return response()->json($message, 400);
            }

            if($request->has('date_limit')){
                $date_limit = $request->date_limit;
            }

            if($request->has('invoice_id')){
                $invoice_id = $request->invoice_id;
            }

              $invoices = $this->invoiceServices->insertInsurence($user_id->id, $date_limit, $invoice_id);

              if(count($invoices) > 0){
                   return response()->json($invoices, 200);
              }else{
                  $message = array('message' => 'Error inserting insurence' );
                   return response()->json($message , 400);
              }
           }else{
                  $message = array('message' => 'needs to have a user_id' );
                  return response()->json($message, 400);
            }
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            $message = array('message' => 'ERROR on the system, try later' );
            return response()->json($message, 400);

        }
    }

   
}