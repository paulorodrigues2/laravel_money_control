<?php
namespace App\Http\Controllers;

use App\Services\InvoiceServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Mail; 
use Config;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject as JWTSubject;
use Illuminate\Support\Facades\Hash;
use App\Repositories\InvoiceRepository;
use App\Models\Stores;


class InvoiceController extends Controller
{
    private $InvoiceServices;

    private $InvoiceRepository;


    public function __construct( InvoiceServices $invoiceServices, InvoiceRepository  $invoiceRepository )
    {
        $this->invoiceServices = $invoiceServices;
        $this->invoiceRepository = $invoiceRepository;
    }

    public function getUserInvoicebyId($invoice_id){
        $invoices = [];
        $message = NULL;
        $user_id = JWTAuth::user();

        try {
           if ($user_id != NULL) {

              $invoices = $this->invoiceServices->findInvoiceUserbyId($user_id->id, $invoice_id);

              if(count($invoices) > 0){
                   return response()->json($invoices, 200);
              }else{
                  $message = array('message' => 'Not found any invoice from user' );
                   return response()->json($message , 400);
              }
           }else{
                  $message = array('message' => 'needs to have a user_id' );
                  return response()->json($message, 400);
            }
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            $message = array('message' => 'ERROR on the system, try later' );
            return response()->json($message, 400);

        }
    }

    public function getInvoicebyCategoryId(Request $request){
        $invoices = [];
        $message = NULL;
        $user_id = JWTAuth::user();

        try {
           if ($user_id != NULL) {

            $validator = Validator::make($request->all(),[
                'category_id' => 'required|integer'
            ]);
    
            if( $validator->fails()){
                $message = array('message' => $validator);
                        return response()->json($message, 400);
            }

            $invoices = $this->invoiceServices->findByCategory($user_id, $request->category_id);

              if( count($invoices) > 0){
                   return response()->json($invoices, 200);
              }else{
                  $message = array('message' => 'Not found any invoice from user' );
                   return response()->json($message , 400);
              }
           }else{
                  $message = array('message' => 'needs to have a user_id' );
                  return response()->json($message, 400);
            }
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            $message = array('message' => 'ERROR on the system, try later' );
            return response()->json($message, 400);

        }
    }

    public function getInvoicebyInvoiceType($category_id){
        $invoices = [];
        $message = NULL;
        $user_id = JWTAuth::user();

        try {
           if ($user_id != NULL) {

            $validator = Validator::make($request->all(),[
                'invoice_type_id' => 'required|integer'
            ]);
    
            if($validator->fails()){
                $message = array('message' => $validator);
                        return response()->json($message, 400);
            }

              $invoices = $this->invoiceServices->findInvoicesByInvoice_Type($user_id, $invoice_type_id);

              if(count($invoices) > 0){
                   return response()->json($invoices, 200);
              }else{
                  $message = array('message' => 'Not found any invoice from user' );
                   return response()->json($message , 400);
              }
           }else{
                  $message = array('message' => 'needs to have a user_id' );
                  return response()->json($message, 400);
            }
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            $message = array('message' => 'ERROR on the system, try later' );
            return response()->json($message, 400);

        }
    }

    /***
     * 
     *  Get All User Invoices
     * 
     */
    public function getUserInvoices(Request $request){
        $invoices = [];
        $message = NULL;
        $user_id = JWTAuth::user();
        $filter_id = $request->get('filter_id');
        $filter = $request->get('filter');
        $order = $request->get('order');
        $page = $request->get('page') != null ? $request->get('page') : 1;
        $type_order = $request->get('type_order');


        try {
           if ($user_id != NULL) {

              $invoices = $this->invoiceServices->findInvoiceUserFilter($user_id->id, $filter, $order, $page, $type_order, $filter_id);

              if(count($invoices) > 0){
                   return response()->json($invoices, 200);
              }else{
                  $message = array('message' => 'Not found any invoice from user' );
                   return response()->json($message , 400);
              }
           }else{
                  $message = array('message' => 'needs to have a user_id' );
                  return response()->json($message, 400);
            }
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            $message = array('message' => 'ERROR on the system, try later' );
            return response()->json($message, 400);

        }
    }

    public function getUserInvoicesofMonth(Request $request){
        $invoices = [];
        $message = NULL;
        $user_id = JWTAuth::user();

        try {
           if ($user_id != NULL) {

            if($request->has('from') && $request->has('to') ){
                $invoices = $this->invoiceServices->findInvoicesofMonth($user_id->id, $from, $to);
            }else{
                $invoices = $this->invoiceServices->findInvoicesofMonth($user_id->id, null, null);
            }


              if(count($invoices) > 0){
                   return response()->json($invoices, 200);
              }else{
                  $message = array('message' => 'Not found any invoice from user' );
                   return response()->json($message , 400);
              }
           }else{
                  $message = array('message' => 'needs to have a user_id' );
                  return response()->json($message, 400);
            }
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            $message = array('message' => 'ERROR on the system, try later' );
            return response()->json($message, 400);

        }
    }

    public function getUserInvoicesCurrentDay(Request $request){
        $invoices = [];
        $message = NULL;
        $user_id = JWTAuth::user();

        try {
           if ($user_id != NULL) {

            if($request->has('day') ){
                $invoices = $this->invoiceServices->findInvoicesDay($user_id->id, $day);
            }else{
                $invoices = $this->invoiceServices->findInvoicesDay($user_id->id, null);
            }


              if(count($invoices) > 0){
                   return response()->json($invoices, 200);
              }else{
                  $message = array('message' => 'Not found any invoice from user' );
                   return response()->json($message , 400);
              }
           }else{
                  $message = array('message' => 'needs to have a user_id' );
                  return response()->json($message, 400);
            }
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            $message = array('message' => 'ERROR on the system, try later' );
            return response()->json($message, 400);

        }
    }

    public function getUserInvoicesofWeek(Request $request){
        $invoices = [];
        $message = NULL;
        $user_id = JWTAuth::user();

        try {
           if ($user_id != NULL) {

            if($request->has('from') && $request->has('to') ){
                $invoices = $this->invoiceServices->findInvoiceWeek($user_id->id, $from, $to);
            }else{
                $invoices = $this->invoiceServices->findInvoiceWeek($user_id->id, null, null);
            }


              if(count($invoices) > 0){
                   return response()->json($invoices, 200);
              }else{
                  $message = array('message' => 'Not found any invoice from user' );
                   return response()->json($message , 400);
              }
           }else{
                  $message = array('message' => 'needs to have a user_id' );
                  return response()->json($message, 400);
            }
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            $message = array('message' => 'ERROR on the system, try later' );
            return response()->json($message, 400);

        }
    }

    public function CreateNewInvoice(Request $request){
        $invoices = [];
        $message = NULL;
        $user_id = JWTAuth::user();

        try {
           if ($user_id != NULL) {

            $object = new \stdClass();
            $object->user_id = $user_id->id;

            if($request->has('name')){
                $object->name = $request->name;
            }

            if($request->has('date_invoice')){
                $object->date_invoice = $request->date_invoice;
            }

            if($request->has('Categories_id')){
                $object->Categories_id = $request->Categories_id;
            }

            if($request->has('Payment_Method_id')){
                $object->Payment_Method_id = $request->Payment_Method_id;
            }

            if($request->has('amount')){
                $object->amount = $request->amount;
            }

            if($request->has('SubCategories_id')){
                $object->SubCategories_id = $request->SubCategories_id;
            }

            if($request->has('Cities_id')){
                $object->Cities_id = $request->Cities_id;
            }

            if($request->has('invoice_type_id')){
                $object->invoice_type_id = $request->invoice_type_id;
            }
            
            if($request->has('Entity_name')){
               $check_exist = Stores::where('name', 'LIKE', "%$request->Entity_name%")->first();

               if($check_exist){
                    $object->Store_name_id = $request->Store_name_id;
               }else{
                    $store = new Stores();
                    $store->name = $request->Entity_name;
                    $store->save();
                    $object->Store_name_id = $store->id;
               }
            }

              $invoices = $this->invoiceServices->createInvoice($object);

              if(count($invoices) > 0){
                   return response()->json($invoices, 200);
              }else{
                  $message = array('message' => 'Error inserting invoice' );
                   return response()->json($message , 400);
              }
           }else{
                  $message = array('message' => 'needs to have a user_id' );
                  return response()->json($message, 400);
            }
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            $message = array('message' => 'ERROR on the system, try later' );
            return response()->json($message, 400);

        }
    }
}