<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class StringIsLengthRule implements Rule
{

    protected $length;
    protected $message = 'The string must be greater than the length';

    public function __construct($length)
    {
        $this->length = $length;
    }
    public function passes($attribute, $value)
    {
        /*business logic for custom validation*/
        if (!$this->length || $this->length <= 0) {
            $this->message = 'The length has to be greater than zero to validate the string';
            return false;
        }
        return strlen($value) > $this->length;
    }

    public function message()
    {
        /*dynamic logic for attribute key object*/
        return $this->message;
    }
}