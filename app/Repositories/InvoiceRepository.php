<?php
namespace App\Repositories;
use App\Models\Invoice;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use App\Helpers\DatesOperations;

class InvoiceRepository
{

    public function create($data)
    {
        $query = new Invoice();
        foreach ($data as $key => $value) {
            $query->$key = $data->$key;
        }
        $query->save();
        return $query;
    }
    /**
     * @param  $applianceId
     * @return 
     */
    public function findByUserInvoiceId($user_id, $invoice_id)
    {
        $array_invoices = [];
        $query = Invoice::where('user_id', $user_id)
        ->where('id', $invoice_id)
        ->with(['invoice_images'])
        ->first();

        if ($query) {
            $object = new \stdClass();
            $object = $query;
            $array_invoices['data'][] = $object;
            
            if(count($query->invoice_images()->get()) > 0){
                $k = 0;
                foreach ($query->invoice_images()->get() as $image) {
                    $array_invoices['data'][0]['invoice_images'][$k] = $image;
                    $k++;
                }
            }
        }
        return $array_invoices;
    }
    
    public function findInvoicesByUserWithFilters($user_id, $filter, $order, $page, $type_order, $filter_id)
    {
        $array_invoices = [];
        $total = 0;
        try {
      
        $query = Invoice::where('user_id', $user_id)
        ->with(['countries', 'subcategories', 'categories', 'cities', 'stores', 'invoice_images', 'payment_methods'])
        ->get();


        if ($query) {
            
            if($order){
            if($order == 'Date_created'){
                $query = $query->orderBy('Date_created', $type_order);
            }else if($order == 'id'){
                $query = $query->orderBy('id', $type_order);
            }
            }
            
            if($filter){
                
                if($filter == 'city'){
                     $k = 0;
                        foreach($query as $invoice){
                            if($invoice->cities->id == intval($filter_id)){
                                $object = new \stdClass();
                                $object = $invoice;
                                $array_invoices['data'][] = $object;
                            }
                            $k++;
                        }
                           
                }else if($filter == 'country'){
                        $k = 0;
                        foreach($query as $invoice){
                                    if($invoice->countries->id == intval($filter_id)){
                                        $object = new \stdClass();
                                        $object = $invoice;
                                        $array_invoices['data'][] = $object;
                                    }
                                    $k++;
                        }
                }else if($filter == 'subcategories'){
                        $k = 0;
                        foreach($query as $invoice){
                                    if($invoice->subcategories->id == intval($filter_id)){
                                        $object = new \stdClass();
                                        $object = $invoice;
                                        $array_invoices['data'][] = $object;
                                    }
                                    $k++;
                        }
                }
                 else if($filter == 'categories'){
                        $k = 0;
                        foreach($query as $invoice){
                                    if($invoice->categories->id == intval($filter_id)){
                                        $object = new \stdClass();
                                        $object = $invoice;
                                        $array_invoices['data'][] = $object;
                                    }
                                    $k++;
                        }
                }
                else if($filter == 'stores'){
                        $k = 0;
                        foreach($query as $invoice){
                                    if($invoice->stores->id == $filter_id){
                                        $object = new \stdClass();
                                        $object = $invoice;
                                        $array_invoices['data'][] = $object;
                                    }
                                    $k++;
                        }
                }else if($filter == 'payment_methods'){
                        $k = 0;
                        foreach($query as $invoice){
                                    if($invoice->payment_methods->id == $filter_id){
                                        $object = new \stdClass();
                                        $object = $invoice;
                                        $array_invoices['data'][] = $object;
                                    }
                                    $k++;
                        }
                }
            }

            $ids = [];
            $l = 0;
            foreach ($array_invoices['data'] as $key => $value) {
               $ids[$l] = $value->id;
               $l++;
            }

           
            $i = 0;
            foreach ($ids as $id) {
                $invoice =  Invoice::where('id', $id)
                ->with(['countries', 'subcategories', 'categories', 'cities', 'stores', 'invoice_images', 'payment_methods'])
                ->first();

                $total = $total + intval($invoice->amount);

                if(count($invoice->invoice_images()->get()) > 0){
                    $k = 0;
                    foreach ($invoice->invoice_images()->get() as $image) {
                        $object = new \stdClass();
                        $object = $image;
                        $array_invoices['data'][$i]['invoice_images'][$k] = $image;
                        $k++;
                    }
                }
                $i++;
            }
        }

       
        $size = count($array_invoices['data']);
        $array_invoices['total'] = $total;

            //code...
        } catch (\Exception $e) {
            \Log::error($e->getLine());
        }

        return $array_invoices;
    }

    public function getInvoicesDayByUser($user_id, $date)
    {
        $total = 0;
        if($date == NULL){
            $current_date = \Carbon\Carbon::today()->format('Y-m-d');
        }else{
            $current_date = $date;
        }

        $array_invoices = [];
        $query = Invoice::where('user_id', $user_id)
        ->whereDate('Date_Created', $current_date)
        ->get();

        if ($query) {
                $k = 0;
               foreach ($query as $invoice) {
                     $object = new \stdClass();
                     $object = $invoice;
                     $array_invoices['invoices'][$k] = $object;
                     $total = $total + $object->amount;
                     $k++;
               }
               $array_invoices['total'] = $total;
        }

        return $array_invoices;
    }

    public function getAllContentInvoicesTodayByUser($user_id)
    {
        $current_date = Carbon::today();
        $array_invoices = [];
        $query = Invoice::where('user_id', $user_id)
        ->whereDate('Date_Created', '=', $current_date)
        ->with(['invoice_images'])
        ->get();

        if ($query) {
            $object = new \stdClass();
            $object = $query;
            $array_invoices['data'][] = $object;
            
            if(count($query->invoice_images()->get()) > 0){
                $k = 0;
                foreach ($query->invoice_images()->get() as $image) {
                    $array_invoices['data'][0]['invoice_images'][$k] = $image;
                    $k++;
                }
            }
        }
        return $query;
    }

    public function findInvoicesByWeekUser($user_id, $from, $to){
        $array_invoices = [];
        $total = 0;
        try {

            $dates_op = new DatesOperations();

            if($from != NULL && $to != NULL){
                $dates = $dates_op->getDaysOfWeek($from, $to);
            }else{
                $from = Carbon::now()->startOfWeek();
                $to = Carbon::now()->endOfWeek();
                $dates = $dates_op->getDaysOfWeek($from, $to);
            }

            /* get all dates of week*/
            $j = 0;
            foreach($dates as $date){
                $total_day = 0;
                $q = Invoice::whereDate('Date_Created', $date )
                ->where('user_id', $user_id)
                ->get();

                if($q){
                    $k = 0;
                    foreach ($q as $invoice) {
                          $total = $total + $invoice->amount;
                          $total_day = $total_day + $invoice->amount;
                          $k++;
                    }
                }
                $array_invoices['invoices'][$j]['total_day']= $total_day;
                $array_invoices['invoices'][$j]['day']= $date;
                $j++;
            }

            $array_invoices['total'] = $total;


        } catch (\Exception $e) {
            \Log::info($e->getMessage());
        }
        return $array_invoices;
    }

    public function findInvoicesByMonthUser($user_id){
        $array_invoices = [];
        $total = 0;
        try {

            $dates_op = new DatesOperations();

            $from = Carbon::now()->startOfMonth();
            $to = Carbon::now()->endOfMonth ();
            $dates = $dates_op->getDaysOfWeek($from, $to);

            /* get all dates of week*/
            $j = 0;
            foreach($dates as $date){
                $total_day = 0;
                $q = Invoice::whereDate('Date_Created', $date )
                ->where('user_id', $user_id)
                ->get();

                if($q){
                    $k = 0;
                    foreach ($q as $invoice) {
                          $total = $total + $invoice->amount;
                          $total_day = $total_day + $invoice->amount;
                          $k++;
                    }
                }
                $array_invoices['invoices'][$j]['total_day']= $total_day;
                $array_invoices['invoices'][$j]['day']= $date;
                $j++;
            }

            $array_invoices['total'] = $total;


        } catch (\Exception $e) {
            \Log::info($e->getMessage());
        }
        return $array_invoices;
    }

    public function findInvoicesByCategory($user_id, $category_id){
        $array_invoices = [];
        $total = 0;
        $dateE = Carbon::now()->startOfMonth(); 

        try {

            $q = Invoice::where('Categories_id', $category_id)
            ->where('user_id', $user_id)
            ->get();
          
           if($q){
               $k = 0;
               foreach ($q as $invoice) {
                     $object = new \stdClass();
                     $object = $invoice;
                     $array_invoices['invoices'][$k] = $object;
                     $total = $total + $object->amount;
                     $k++;
               }
               $array_invoices['total'] = $total;
           }

        } catch (\Exception $e) {
            \Log::info($e->getMessage());
        }
        return $array_invoices;
    }

    public function findInvoicesByInvoice_Type($user_id, $invoice_type_id){
        $array_invoices = [];
        $total = 0;

        try {

            $q = Invoice::where('invoice_type_id', $invoice_type_id)
            ->where('user_id', $user_id)
            ->get();
          
           if($q){
               $k = 0;
               foreach ($q as $invoice) {
                     $object = new \stdClass();
                     $object = $invoice;
                     $array_invoices['invoices'][$k] = $object;
                     $total = $total + $object->amount;
                     $k++;
               }
               $array_invoices['total'] = $total;
           }

        } catch (\Exception $e) {
            \Log::info($e->getMessage());
        }
        return $array_invoices;
    }
    
   
}