<?php
namespace App\Repositories;
use App\Models\Insurence_Invoice;
use App\Repositories\BaseRepository;
use Carbon\Carbon;

class Insurence_Invoice_Repository
{
    /**
     * @param array $data
     * @param bool $provider
     *
     * @return static
     */
    public function create($user_id, $date_limit , $invoice_id)
    {
        $isSaved = false;
        try {
            $invoice = new Insurence_Invoice();
            $invoice->user_id = $user_id;
            $invoice->date_limit  = $date_limit;
            $invoice->invoice_id  = $invoice_id;
            if($invoice->save()){
                $isSaved = true;
            }
            
        } catch (\Exception $e) {
           \Log::info($e);
        }
        return $isSaved;
    }

    public function GetInsurencesByUser($user_id)
    {
        $insurences_array = [];
        try {
            $insurences = Insurence_Invoice::where('user_id', $user_id)->get();
          
            if($insurences){
                $i = 0;
                foreach($insurences as $insurence){
                    $insurences_array[$i] = $insurence;
                    $i++;
                }
            }
            
        } catch (\Exception $e) {
           \Log::info($e);
        }
        return $insurences_array;
    }

    public function GetInsurencesById($user_id, $insurence_id)
    {
        $object = new \stdClass();

        try {
            $insurences = Insurence_Invoice::where('id', $insurence_id)
            ->where('user_id', $user_id)->first();
          
            if($insurences){
                $object = $insurences;
            }
            
        } catch (\Exception $e) {
           \Log::info($e);
        }
        return $insurences;
    }

    public function GetInsurencesEnding($insurence_id)
    {
        //$object = new \stdClass();
        $array_insurences = [];
        $current_date = Carbon::today();

        try {
            $insurences = Insurence_Invoice::whereDate('date_limit', $current_date)->get();

            if($insurences){
                $i = 0;
                foreach($insurences as $incurence){
                    $array_insurences[$i] = $insurence;
                    $i++;
                }
            }

            if(count($array_insurences) > 0){

                /*send notifications to user that insurence have finished*/

            }
            
        } catch (\Exception $e) {
           \Log::info($e);
        }
        return $insurences;
    }
   
}