<?php
namespace App\Repositories;
use App\Models\Invoice_Images;
use App\Repositories\BaseRepository;
class InvoiceImagesRepository
{
    /**
     * @param array $data
     * @param bool $provider
     *
     * @return static
     */
    public function create($user_id, $image_path, $invoice_id)
    {
        $isSaved = false;
        try {
            $invoice = new Invoice_Images();
            $invoice->user_id = $user_id;
            $invoice->name_url  = $image_path;
            $invoice->Invoice_id  = $invoice_id;
            if($invoice->save()){
                $isSaved = true;
            }
            
        } catch (\Exception $e) {
           \Log::info($e);
        }
        return $isSaved;
    }
   
}